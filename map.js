var map, marker
function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        center: {
            lat: 14,
            lng: 100
        },
    });
    map.addListener("click", (e) => {
        placeMarkerAndPanTo(e.latLng, map);
    });
}

function placeMarkerAndPanTo(latLng, map) {
    // console.log('latLng :>> ', latLng);
    document.getElementById('latitude').value = latLng.lat()
    document.getElementById('longitude').value = latLng.lng()

}